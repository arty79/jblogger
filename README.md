# JBlogger
A simple blog application using SpringBoot.

Run the application using `mvn spring-boot:run`

Access Application: http://localhost:8080/


Login: admin@gmail.com
pass: admin


### Running Application using docker-compose

`jblogger> docker-compose up`

Access Application: http://localhost:9090

http://localhost:9090/swagger-ui.html#
admin@gmail.com /admin
siva@gmail.com /siva
